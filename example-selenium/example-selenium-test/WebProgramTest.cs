using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace example_selenium_test;

public class WebProgramTest
{
    private IWebDriver? _driver;
    
    [SetUp]
    public void Setup()
    {
        var chromeOptions = new ChromeOptions();
        chromeOptions.AddArguments("headless");
        _driver = new ChromeDriver(chromeOptions);
    }

    [TearDown]
    public void TearDown()
    {
        _driver.Quit();
    }

    [Test]
    public void CheckTitleTest()
    {
        _driver!.Navigate().GoToUrl("http://127.0.0.1:5151");
        var e = _driver.FindElement(By.TagName("p"));
        //e.Click();
        Assert.AreEqual("Learn about building Web apps with ASP.NET Core.", e.Text);
    }
}